const mongoose = require("mongoose");

const { Schema } = mongoose;

const productSchema = new Schema({
  name: { type: String, required: true },
  desc: String,
  price: Number,
  imageCover: String,
  comments: [{ body: String, date: { type: Date, default: Date.now } }],
  date: { type: Date, default: Date.now },
});

const Products = (module.exports = mongoose.model("products", productSchema));

module.exports.createProduct = (newProduct, callback) => {
  newProduct.save(callback);
};

module.exports.getAllProduct = (data) => {
  Products.find(data);
};

module.exports.deleteIdProduct = (id, callback) => {
  Products.deleteOne({ _id: id }, callback);
};

module.exports.getProductId = (id, callback) => {
  Products.findOne({ _id: id }, callback);
};

module.exports.updateProductId = (id, data, callback) => {
  Products.updateOne({ _id: id }, data, callback);
};

module.exports.getProductsPagination = (
  query = {},
  page = 1,
  limit = 5,
  sort = {},
  callback
) => {
  const data_page = parseInt(page) || 1;
  const data_limit = parseInt(limit) || 5;
  Products.find(query)
    .skip((data_page - 1) * limit) //Notice here
    .limit(data_limit)
    .sort(sort)
    .exec((err, doc) => {
      if (err) console.log("err", err);
      Products.countDocuments(query).exec((count_error, count) => {
        if (count_error) console.log("err", count_error);
        return callback(null, {
          total: count,
          page: page,
          pageSize: doc.length,
          productDatas: doc,
        });
      });
    });
};
