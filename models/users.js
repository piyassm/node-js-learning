const mongoose = require("mongoose");

const { Schema } = mongoose;

const userSchema = new Schema({
  username: { type: String, required: true, unique: true, index: true },
  password: { type: String, required: true },
  name: { type: String, required: true },
  created_date: { type: Date, default: Date.now },
});

const Users = (module.exports = mongoose.model("users", userSchema));

module.exports.createUser = (newUser, callback) => {
  newUser.save(callback);
};

module.exports.getAllUser = (data) => {
  Users.find(data);
};

module.exports.deleteIdUser = (id, callback) => {
  Users.deleteOne({ _id: id }, callback);
};

module.exports.getUserId = (id, callback) => {
  Users.findOne({ _id: id }, callback);
};

module.exports.getUsername = (username, callback) => {
  Users.findOne({ username: username }, callback);
};

module.exports.updateUserId = (id, data, callback) => {
  Users.updateOne({ _id: id }, data, callback);
};
