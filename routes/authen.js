var express = require("express");
var router = express.Router();

const authenController = require("../controllers/authen");

const checkLoggedIn = [authenController.ifLoggedIn];
const checkLoggedInAndValidFormLogin = [
  ...checkLoggedIn,
  authenController.middlewareFormLogin,
];
const checkLoggedInAndValidFormRegister = [
  ...checkLoggedIn,
  authenController.middlewareFormRegister,
];
const middlewareLogin = [authenController.middleLogin];

router.get("/authen", checkLoggedIn, authenController.authPage);

router.get(
  ["/login", "/register", "/logout"],
  checkLoggedIn,
  authenController.authPageRedirect
);

router.post(
  "/login",
  checkLoggedInAndValidFormLogin,
  authenController.submitLogin,
  middlewareLogin
);

router.post(
  "/register",
  checkLoggedInAndValidFormRegister,
  authenController.submitRegister,
  middlewareLogin
);


module.exports = router;
