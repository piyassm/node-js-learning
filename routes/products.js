var express = require("express");
var router = express.Router();

const productController = require("../controllers/product");

router.get("/", productController.indexPage);

router.get("/add", function (req, res, next) {
  res.render("products/add", { title: "Express" });
});

router.post(
  "/add",
  productController.middlewareProduct,
  productController.createProduct
);

router.post("/delete", productController.deleteProduct);

router.get("/edit/:id", productController.updatePage);

router.post(
  "/edit",
  productController.middlewareProduct,
  productController.updateProduct
);

module.exports = router;
