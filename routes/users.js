var express = require("express");
var router = express.Router();
var Users = require("../models/users");

router.get("/", function (req, res, next) {
  const id = req.params.id;
  if (id) {
    Users.getUserId((err, userData) => {
      if (err) console.log("err", err);
      res.render("users/index", { title: "Express", userData });
    });
  } else {
    res.redirect("/");
  }
});

module.exports = router;
