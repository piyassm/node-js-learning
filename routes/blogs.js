var express = require("express");
var router = express.Router();

const blogController = require("../controllers/blog");

router.get("/", blogController.indexPage);

router.get("/add", function (req, res, next) {
  res.render("blogs/add", { title: "Express" });
});

router.post("/add", blogController.middlewareBlog, blogController.createBlog);

module.exports = router;
