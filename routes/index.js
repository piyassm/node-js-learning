var express = require("express");
var router = express.Router();

const authenController = require("../controllers/authen");
var Products = require("../models/products");

/* GET home page. */
router.get("/", function (req, res, next) {
  // res.render("index", { title: "Node Js Leaning @mamaiank" });
  var page = parseInt(req.query.page) || 1;
  var limit = parseInt(req.query.limit) || 3;
  var query = {};
  Products.getProductsPagination(query, 0, 0, {}, async (err, product) => {
    if (err) console.log("err", err);
    return res.render("products/index", product);
  });
});

router.post("/logout", function (req, res, next) {
  req.logout();
  res.redirect("/");
});

module.exports = router;
