var Products = require("../models/products");
const { check, validationResult } = require("express-validator");

const uploadImg = async (productImg, id) => {
  let image_name;
  let fileExtension;
  if (productImg) {
    image_name = productImg.name;
    fileExtension = productImg.mimetype.split("/")[1];
  }

  if (
    productImg.mimetype === "image/png" ||
    productImg.mimetype === "image/jpeg" ||
    productImg.mimetype === "image/jpg"
  ) {
    image_name = id + "." + fileExtension;
    Products.updateProductId(
      id,
      {
        imageCover: image_name,
      },
      (err) => {
        if (err) console.log("err", err);
      }
    );
    await productImg.mv(`public/assets/img/${image_name}`, (err) => {
      if (err) console.log("err", err);
    });
  }
  return true;
};

exports.middlewareProduct = [
  check("name", "Please Input Name").not().isEmpty(),
  check("desc", "Please Input Desc").not().isEmpty(),
  check("price", "Please Input Price").not().isEmpty(),
  check("price", "Price Invalid").isNumeric(),
];

exports.indexPage = function (req, res, next) {
  Products.getAllProduct((err, productDatas) => {
    if (err) console.log("err", err);
    res.render("products/index", { title: "Express", productDatas });
  });
};

exports.createProduct = function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render("products/add", {
      errors: errors.errors,
      products: req.body,
    });
  }

  const { name, desc, price } = req.body;
  const { productImg } = req.files || {};

  const data = new Products({
    name,
    desc,
    price,
  });
  Products.createProduct(data, async (err, product) => {
    if (err) console.log("err", err);
    !!productImg && (await uploadImg(productImg, product.id));
    return res.redirect("/products");
  });
};

exports.deleteProduct = function (req, res, next) {
  const id = req.body.id;
  if (id) {
    Products.deleteIdProduct(id, (err) => {
      if (err) console.log("err", err);
      res.redirect("/products");
    });
  } else {
    res.redirect("/");
  }
};

exports.updatePage = function (req, res, next) {
  const id = req.params.id;
  if (id) {
    Products.getProductId(id, (err, products) => {
      if (err) console.log("err", err);
      res.render("products/edit", { title: "Express", products });
    });
  } else {
    res.redirect("/");
  }
};

exports.updateProduct = function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render("products/edit", {
      errors: errors.errors,
      products: req.body,
    });
  }

  const id = req.body.id;
  const { productImg } = req.files || {};
  if (id) {
    const { name, desc, price } = req.body;
    Products.updateProductId(
      id,
      {
        name,
        desc,
        price,
      },
      async (err, product) => {
        if (err) console.log("err", err);
        !!productImg && (await uploadImg(productImg, id));
        res.redirect("/products");
      }
    );
  } else {
    res.redirect("/");
  }
};
