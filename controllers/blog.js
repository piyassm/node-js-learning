const { check, validationResult } = require("express-validator");
var env = require("../env");
const url = env.MONGO_URI;
const db = require("monk")(url);
db.catch(function (err) {
  console.log("monk err", err);
});

exports.middlewareBlog = [
  check("name", "Please Input Name").not().isEmpty(),
  check("desc", "Please Input Desc").not().isEmpty(),
  check("author", "Please Input Author").not().isEmpty(),
];

exports.indexPage = function (req, res, next) {
  const blogs = db.get("blogs");
  let blogDatas = [];
  blogs
    .find({})
    .then(function (docs) {
      blogDatas = (!!docs?.length && docs) || [];
      res.render("blogs/index", { title: "Express", blogDatas });
    })
    .catch((err) => {
      console.log("get err", err);
    })
    .then(() => db.close());
};

exports.createBlog = function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render("blogs/add", {
      errors: errors.errors,
      blogs: req.body,
    });
  }
  const blogs = db.get("blogs");
  const { name, desc, author } = req.body;
  blogs
    .insert({
      blog_name: name,
      blog_desc: desc,
      blog_author: author,
    })
    .then((docs) => {
      res.redirect("/blogs");
    })
    .catch((err) => {
      console.log("insert err", err);
    })
    .then(() => db.close());
};
