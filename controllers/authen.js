const { check, validationResult } = require("express-validator");
var Users = require("../models/users");
const util = require("../lib/helpers/util");
var passport = require("passport");

exports.ifLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }
  return next();
};

exports.middleLogin = (req, res, next) => {
  passport.authenticate("local", function (err, user, info) {
    if (err) return next(err);
    if (!user) {
      req.session.loginError = [
        {
          msg: "Username or Password Invalid.",
        },
      ];
      return res.redirect("/authen");
    }
    req.logIn(user, function (err) {
      if (err) return next(err);
      return res.redirect("/");
    });
  })(req, res, next);
};

exports.middlewareFormLogin = [
  check("username", "Please Input Username").not().isEmpty(),
  check("password", "Please Input Password").not().isEmpty(),
];
exports.middlewareFormRegister = [
  check("name", "Please Input Name").not().isEmpty(),
  check("username", "Please Input Username")
    .not()
    .isEmpty()
    .custom((value) => {
      return Users.findOne({ username: value }).then((user) => {
        if (user) {
          return Promise.reject("Username already in use");
        }
      });
    }),
  check("password", "Please Input Password").not().isEmpty(),
  check("passwordConfirm", "Please Input Confirm Password")
    .not()
    .isEmpty()
    .custom((value, { req }) => {
      const { password } = req.body;
      if (value !== password) {
        return Promise.reject("Password confirmation is incorrect");
      } else {
        return true;
      }
    }),
];

exports.authPage = function (req, res, next) {
  const { registerError, loginError } = req.session;
  let registerErrorMsg;
  let loginErrorMsg;
  if (registerError) {
    registerErrorMsg = registerError;
    req.session.registerError = null;
  } else if (loginError) {
    loginErrorMsg = loginError;
    req.session.loginError = null;
  }
  return res.render("authen", { registerErrorMsg, loginErrorMsg });
};

exports.authPageRedirect = function (req, res, next) {
  return res.redirect("/users");
};

exports.submitLogin = function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = errors.errors;
    req.session.loginError = error;
    return res.redirect("/authen");
  }
  return next();
};

exports.submitRegister = async function (req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const error = errors.errors;
    req.session.registerError = error;
    return res.redirect("/authen");
  }
  const { username, password, name } = req.body;
  const hashPassword = await util.setHashPassword(password);
  const data = new Users({
    username,
    password: hashPassword,
    name,
  });
  Users.createUser(data, (err, user) => {
    if (err) console.log("err", err);
    return next();
  });
};
