const bcrypt = require("bcrypt");


async function setHashPassword(password) {
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);
  return hash;
}

async function checkPassword(password, hash) {
  const isSame = await bcrypt.compare(password, hash);
  return isSame;
}

module.exports.setHashPassword = setHashPassword;
module.exports.checkPassword = checkPassword;
