const MONGO_USER = process.env.MONGO_USER || "root";
const MONGO_PW = process.env.MONGO_PW || "password";
const MONGO_URL = process.env.MONGO_URL || "mongodb:27017";
const MONGO_DB_NAME = process.env.MONGO_DB_NAME || "nodejs?authSource=admin";
const MONGO_PATH = `${MONGO_USER}:${MONGO_PW}@${MONGO_URL}/${MONGO_DB_NAME}`;
const MONGO_CONNECT_TYPE = process.env.MONGO_CONNECT_TYPE || "mongodb://";
const MONGO_URI = MONGO_CONNECT_TYPE + MONGO_PATH;
const env = {
  MONGO_URI,
  MONGO_PATH,
};
module.exports = env;
