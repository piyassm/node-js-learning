var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var session = require("express-session");
const fileUpload = require('express-fileupload');

const util = require("./lib/helpers/util");
var Users = require("./models/users");
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;

passport.use(
  new LocalStrategy(function (username, password, done) {
    Users.findOne({ username: username }, async function (err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false, {
          message: "Incorrect username or password.",
        });
      }
      const checkPassword = await util.checkPassword(password, user.password);
      if (!checkPassword) {
        return done(null, false, {
          message: "Incorrect username or password.",
        });
      }
      return done(null, user);
    });
  })
);

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  Users.findById(id, function (err, user) {
    done(err, user);
  });
});

require("./db");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var blogsRouter = require("./routes/blogs");
var productsRouter = require("./routes/products");
var authensRouter = require("./routes/authen");

var app = express();
app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true,
    cookie: {
      // secure: process.env.NODE_ENV === "development" ? false : true,
      maxAge: 2 * 24 * 60 * 60 * 1000,
    },
  })
);
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(fileUpload()); // configure fileupload

app.use(passport.initialize());
app.use(passport.session());

const ifNotLoggedIn = (req, res, next) => {
  if (!req.isAuthenticated()) {
    return res.redirect("/authen");
  }
  return next();
};

const ifLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }
  return next();
};

const setSessionToLocal = (req, res, next) => {
  res.locals.userID = "";
  res.locals.userName = "";
  if (req?.user) {
    const { _id, username } = req.user;
    res.locals.userID = _id;
    res.locals.userName = username;
  }
  return next();
};

const setDataLocalFromSession = [setSessionToLocal];
const checkNotLoggedIn = [ifNotLoggedIn];
const checkLoggedIn = [ifLoggedIn];
const checkNotLoggedInAndSetSession = [
  ...checkNotLoggedIn,
  ...setDataLocalFromSession,
];
const checkLoggedInAndSetSession = [
  ...checkLoggedIn,
  ...setDataLocalFromSession,
];

app.use("/", setDataLocalFromSession, indexRouter);
app.use("/users", checkNotLoggedInAndSetSession, usersRouter);
app.use("/blogs", checkNotLoggedInAndSetSession, blogsRouter);
app.use("/products", checkNotLoggedInAndSetSession, productsRouter);
app.use("/", checkLoggedInAndSetSession, authensRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
